# Snail

Snail is a collection of project templates to get projects into deployment fast while being maintainable.

Main principles:
1. As must as possible of infrastructure definitions are documented as code
2. Easy dev, staging and prod environments